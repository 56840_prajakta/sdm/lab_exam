create table movie(
    movie_id int primary key auto_increment,
    movie_title varchar(50),
    movie_release_date date,
    movie_time time,
    director_name varchar(50)
);

insert into movie values (default, 'yjhd', '2022-03-15', '03:02:01', 'raj');
insert into movie values (default, 'znmd', '2020-01-05', '02:01:11', 'rahul');
insert into movie values (default, 'ddlj', '2019-11-08', '01:11:01', 'raj');